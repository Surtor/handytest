package ru.iscras.handytest;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.LinearLayout;

/**
 * ����� ���������, ��� SView(����������� �������� � ���� View).
 * 
 * @author Ratushnyak Sergey
 *
 */

public class MainView extends LinearLayout {
	private List<SView> sViews = new ArrayList<SView>();
	private boolean showFPS = false;

	public MainView(Context context) {
		super(context);
	}

	/**
	 * ������� ��� ����������� ������ ���������� SView.
	 * 
	 */
	public void removeAllSview() {
		removeAllViews();
		sViews.clear();
	}

	/**
	 * 
	 * @return ���������� ������� FPS(frame per second) ������������ SView.
	 */
	public float getAverageFPS() {
		float summFps = 0;
		for (SView s : sViews) {
			summFps += s.getFPS();
		}
		return ((long) ((summFps / sViews.size()) * 100f)) / 100f;

	}

	/**
	 * ������������ ��� ���� SView ����� FPS.
	 * 
	 * @return ���� ������� ���������� ���������� true, ����� false.
	 */
	public boolean showFPS() {
		showFPS = true;
		boolean fResult = false;// result of this function
		for (SView s : sViews) {
			boolean isShow = s.showFPS();
			fResult = (isShow || fResult);
		}
		return fResult;
	}

	/**
	 * ������������� ��� ���� SView ������� FPS.
	 * 
	 * @return ���� ������� ���������� ���������� true, ����� false.
	 */
	public boolean hideFPS() {
		showFPS = false;
		boolean fResult = false;// result of this function
		for (SView s : sViews) {
			boolean isHide = s.hideFPS();
			fResult = (isHide || fResult);
		}
		return fResult;
	}

	/**
	 * ������� ��������� ��������� SView.
	 * 
	 * @return ���������� true ���� �������, ����� false.
	 */
	public boolean removeLastSView() {
		if (sViews.size() > 0) {
			this.removeViewInLayout(sViews.get(0));
			sViews.remove(0);

			return true;
		}
		return false;
	}

	/**
	 * ��������� SView � ��������.
	 * 
	 * @return ���������� true ���� �������, ����� false.
	 */
	public boolean addSView() {
		SView newSView = new SView(this.getContext());
		if (sViews.size() < 10) {
			sViews.add(newSView);
			if (showFPS)
				newSView.showFPS();
			LayoutParams paramsForSView = new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			paramsForSView.weight = 1;
			addView(newSView, paramsForSView);
			return true;
		}
		return false;
	}
}
