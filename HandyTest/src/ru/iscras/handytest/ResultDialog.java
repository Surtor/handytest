package ru.iscras.handytest;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * ����� ���������� �������, ��� ������ ����������.
 * 
 * @author Ratushnyak Sergey
 *
 */
class ResultDialog extends DialogFragment implements OnClickListener {
	private final float result;

	public ResultDialog(float result) {
		this.result = result;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setTitle(getResources().getString(R.string.result_dialog));
		View v = inflater.inflate(R.layout.result_dialog_layout, container);
		v.findViewById(R.id.btnOk).setOnClickListener(this);

		TextView T = (TextView) v.findViewById(R.id.result_dialog_textview);
		String resultDialogMessage = getResources().getString(
				R.string.result_dialog_message);
		String resultDialogFPS = getResources().getString(
				R.string.result_dialog_fps);
		T.setText(resultDialogMessage + " " + result + " " + resultDialogFPS);
		return v;
	}

	@Override
	public void onClick(View v) {
		dismiss();
	}

	@Override
	public void onPause() {
		super.onPause();
		dismiss();
	}
}
