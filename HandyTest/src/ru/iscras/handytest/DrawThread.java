package ru.iscras.handytest;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.SurfaceHolder;

/**
 * ����� ������ ��������� ��������.
 * @author Ratushnyak Sergey
 *
 */
/**
 * @author Ratushnyak Sergey
 *
 */
class DrawThread extends Thread {

	private volatile boolean runFlag = true;
	private volatile boolean showFPS = false;
	private volatile float framePerSecond;
	private SurfaceHolder surfaceHolder;
	private ImageInfo firstPicture, secondPicture, currentImage;
	private float imageCoeff = 1;// ����������� ��������� ��������
	private volatile float imageChangeCoeff = -0.05f;// �������� ���������
														// ������������
	private long frames = 0;

	/**
	 * ����� ��� ���������� ������ � �������� � ��������� ������� �������� ��
	 * ������������ ��������.
	 * 
	 * @author Ratushnyak Sergey
	 *
	 */
	private final static class ImageInfo {
		private int xCenter;
		private int yCenter;
		private Bitmap image;
		private Matrix fitToSurface;

		public ImageInfo(int xCenter, int yCenter, Bitmap image,
				Matrix fitToSurface) {
			this.xCenter = xCenter;
			this.yCenter = yCenter;
			this.image = image;
			this.fitToSurface = fitToSurface;
		};

		public Bitmap getImage() {
			return image;
		};

		public Matrix getMatrixFitToSurface() {
			return fitToSurface;
		}

		/**
		 * @param changeCoeff
		 *            �������� �������� ��������
		 * @return ������� ��������
		 */
		public Matrix getChangeXMatrix(float changeCoeff) {
			Matrix changeMatrix = new Matrix();
			changeMatrix.postScale(changeCoeff, 1f, xCenter, yCenter);

			Matrix returnMatrix = new Matrix(getMatrixFitToSurface());
			returnMatrix.postConcat(changeMatrix);
			return returnMatrix;
		}
	};

	/**
	 * ��������� �������� �������� ��������.
	 * 
	 * @param imChangeCoeff
	 *            �������� �������� �������� �� 0 �� 10.
	 * @return ������� �������� ��������,���� ������� ����������, ����� 0.
	 */
	public int setImageRotateSpeed(int imChangeCoeff) {
		if ((imChangeCoeff > 0) && (imChangeCoeff <= 10)) {
			imageChangeCoeff = -0.01f * imChangeCoeff;
			return imChangeCoeff;
		}
		return 0;
	};

	/**
	 * ���������� FPS(���������� ������ � �������).
	 * 
	 * @return true, ���� ������� ���������� ���� FPS � true
	 */
	public boolean showFPS() {
		showFPS = true;
		return showFPS;

	};

	/**
	 * �������� FPS (���������� ������ � �������).
	 * 
	 * @return true, ���� ������� ���������� ���� FPS � false
	 */
	public boolean hideFPS() {
		showFPS = false;
		return !showFPS;
	};

	/**
	 * ������� �������� FPS
	 * 
	 * @return ���������� ������� �������� FPS
	 */
	public float getFPS() {
		return framePerSecond;
	};

	private void swapCurrentImage() {
		if (currentImage == firstPicture) {
			currentImage = secondPicture;
		} else {
			currentImage = firstPicture;
		}
	}

	private float getNextCoeff() {

		if (imageCoeff <= 0 || imageCoeff > 1) {
			if (imageCoeff <= 0)
				swapCurrentImage();

			imageChangeCoeff = -imageChangeCoeff;

		}
		imageCoeff += imageChangeCoeff;

		return imageCoeff;
	}

	private ImageInfo CreateImageInfo(Bitmap image) {
		Matrix fitToSurface = new Matrix();
		int xImageCenter, yImageCenter;

		float surfaceCoeff = (float) surfaceHolder.getSurfaceFrame().height()
				/ surfaceHolder.getSurfaceFrame().width();

		float imagePictureCoeff = (float) image.getHeight() / image.getWidth();
		float fitCoeffFirstPicture;

		if (imagePictureCoeff >= surfaceCoeff) {
			fitCoeffFirstPicture = (float) surfaceHolder.getSurfaceFrame()
					.height() / (float) image.getHeight();
			fitToSurface.postTranslate(surfaceHolder.getSurfaceFrame().width()
					/ 2 - image.getWidth() * fitCoeffFirstPicture / 2, 0);
		} else {
			fitCoeffFirstPicture = (float) surfaceHolder.getSurfaceFrame()
					.width() / (float) image.getWidth();
			fitToSurface.postTranslate(0, surfaceHolder.getSurfaceFrame()
					.height()
					/ 2
					- image.getHeight()
					* fitCoeffFirstPicture
					/ 2);
		}

		xImageCenter = surfaceHolder.getSurfaceFrame().width() / 2;
		yImageCenter = surfaceHolder.getSurfaceFrame().height() / 2;

		fitToSurface.preScale(fitCoeffFirstPicture, fitCoeffFirstPicture);

		return new ImageInfo(xImageCenter, yImageCenter, image, fitToSurface);
	}

	/**
	 * ����������� ������ ���������.
	 * 
	 * @param surfaceHolder
	 *            ����������� ���������.
	 * @param resources
	 *            ������ ���������� ��� ��������� ��������.
	 */
	public DrawThread(SurfaceHolder surfaceHolder, Resources resources) {
		this.surfaceHolder = surfaceHolder;

		// ������� ��������� ��������, ��� ����� ������� ���������
		Bitmap firstBitmap = BitmapFactory.decodeResource(resources,
				R.drawable.firstp);
		Bitmap secondBitmap = BitmapFactory.decodeResource(resources,
				R.drawable.jacket);
		firstPicture = CreateImageInfo(firstBitmap);
		secondPicture = CreateImageInfo(secondBitmap);
		currentImage = firstPicture;
	}

	/**
	 * ���� ������ ������
	 * 
	 * @param run
	 *            true-����� ��������, false-����� �����������
	 */
	public void setRunning(boolean run) {
		runFlag = run;
	}

	@Override
	public void run() {
		Canvas canvas;
		long startDrawing = System.currentTimeMillis();// ����� ������ ���������
		long now;// ������� ������ �������
		Paint paint = new Paint();

		int textSize = (int) (surfaceHolder.getSurfaceFrame().height() * 0.05);
		int textColor = -1;

		paint.setColor(textColor);
		paint.setTextSize(textSize);

		while (runFlag && !isInterrupted()) {

			canvas = null;
			try {
				canvas = surfaceHolder.lockCanvas();
				if (canvas != null) {
					canvas.drawColor(Color.BLACK);

					if (showFPS)
						canvas.drawText(
								Float.toString((long) (framePerSecond * 100) / 100f),
								0, textSize, paint);

					Matrix rotateMatrix = currentImage
							.getChangeXMatrix(getNextCoeff());
					Bitmap currentTempBitmap = currentImage.getImage();
					canvas.drawBitmap(currentTempBitmap, rotateMatrix, null);
				}
			} finally {
				if (canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
					frames++;
				}
			}
			now = System.currentTimeMillis();
			framePerSecond = (frames / ((now - startDrawing) / 1000f));
		}
	}
}
