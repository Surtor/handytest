package ru.iscras.handytest;

import java.util.Random;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * @author Ratushnyak Sergey ����� View ��� ��������� � ������� ������.
 *
 */
class SView extends SurfaceView implements SurfaceHolder.Callback {
	private DrawThread drawThread;
	private boolean showFPS = false;

	public SView(Context context) {
		super(context);
		getHolder().addCallback(this);
		drawThread = null;
	}

	/**
	 * 
	 * @return ���������� FPS(frame per second) ������, ���� ����� ��� �� �����
	 *         ���������� -1;
	 */
	public float getFPS() {
		if (drawThread != null)
			return drawThread.getFPS();
		return -1;
	}

	/**
	 * �������� ������ � ������������� ������� FPS(frame per second).
	 * 
	 * @return ���������� true, ���� ������� ������������, � ��������� ������
	 *         false.
	 */
	public boolean showFPS() {
		showFPS = true;
		if (drawThread != null)
			return drawThread.showFPS();
		return false;
	}

	/**
	 * �������� ������, ��� ����������� ������ FPS(frame per second)
	 * 
	 * @return ���������� true, ���� ������� ������������, � ��������� ������
	 *         false.
	 */
	public boolean hideFPS() {
		showFPS = false;
		if (drawThread != null)
			return drawThread.hideFPS();
		return false;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

		drawThread = new DrawThread(getHolder(), getResources());
		drawThread.setRunning(true);

		Random r = new Random();
		drawThread.setImageRotateSpeed(3 + r.nextInt(5));

		if (showFPS)
			drawThread.showFPS();

		drawThread.start();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		// ��������� ������ ������
		drawThread.setRunning(false);
		while (retry) {
			try {
				drawThread.join();
				retry = false;
			} catch (InterruptedException e) {

			}
		}

	}

}
