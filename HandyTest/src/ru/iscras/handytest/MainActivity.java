package ru.iscras.handytest;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

public class MainActivity extends ActionBarActivity {
	private MainView mainView = null;
	private boolean fourCards = false;
	private Menu menu = null;
	private Handler handler;
	private final int testTime = 10000;// ����� ������ �����
	private Runnable stopTestRunnable = new Runnable() {// ����� ��� ���������
														// �����
		public void run() {
			stopTest();
		}
	};
	private Runnable startTestRunnable = new Runnable() {// ����� ��� ������
															// �����
		public void run() {
			startTestInThread();
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		handler.removeCallbacks(stopTestRunnable);
		handler.removeCallbacks(startTestRunnable);
		mainView.removeAllSview();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mainView.addSView();
		setContentView(mainView);
		if (menu != null)
			menu.findItem(R.id.action_start_test).setEnabled(true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handler = new Handler();
		mainView = new MainView(this);
		setTitle(getResources().getString(R.string.app_name));
	}

	/**
	 * ������� ������� ����� �� ������.
	 */
	private void startTestInThread() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mainView.removeAllSview();
		int quantityCards = 2;
		if (fourCards)
			quantityCards = 4;
		for (; quantityCards > 0; quantityCards--)
			mainView.addSView();
		String title = getResources().getString(R.string.app_name);
		String waitMessage = getResources().getString(
				R.string.main_activity_wait_message);
		setTitle(title + " " + waitMessage);

		setContentView(mainView);
		handler.postDelayed(stopTestRunnable, testTime);
	}

	/**
	 * ������ �����.
	 */
	private void startTest() {
		menu.findItem(R.id.action_start_test).setEnabled(false);
		handler.post(startTestRunnable);

	}

	/**
	 * ������� ��������� ����� �� ������.
	 */
	private void stopTest() {
		String resultDialogTAG = getResources().getString(
				R.string.result_dialog);
		new ResultDialog(mainView.getAverageFPS()).show(
				getSupportFragmentManager(), resultDialogTAG);
		mainView.removeAllSview();
		mainView.addSView();
		setContentView(mainView);
		menu.findItem(R.id.action_start_test).setEnabled(true);
		setTitle(getResources().getString(R.string.app_name));
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	/**
	 * ����� ������� ���� � ����.
	 * 
	 * @param item
	 *            ����� � ����.
	 */
	private void fourCardSelected(MenuItem item) {
		if (item.isChecked()) {
			fourCards = false;
			item.setChecked(false);
		} else {
			fourCards = true;
			item.setChecked(true);
		}
	}

	/**
	 * ����� ������ FPS � ����.
	 * 
	 * @param item
	 *            ����� � ����.
	 */
	private void showFPSSelected(MenuItem item) {
		if (item.isChecked()) {
			item.setChecked(false);
			mainView.hideFPS();
		} else {
			item.setChecked(true);
			mainView.showFPS();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		this.menu = menu;
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_show_fps) {
			showFPSSelected(item);
			return true;
		}
		if (id == R.id.action_start_test) {
			startTest();
			return true;
		}
		if (id == R.id.action_four_cards) {
			fourCardSelected(item);
			return true;
		}
		return super.onOptionsItemSelected(item);

	}

}
